import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class FormPage extends StatefulWidget {
  const FormPage({Key? key}) : super(key: key);
  @override
  FormPageState createState() => FormPageState();
}

class FormPageState extends State<FormPage> {
  //final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController emailController = TextEditingController();
    TextEditingController numberController = TextEditingController();

    Future _addSubmissions() {
      final name = nameController.text;
      final email = emailController.text;
      final number = numberController.text;

      final ref = FirebaseFirestore.instance.collection("submissions").doc();

      return ref
          .set({
            "Full_Name": name,
            "Email_Address": email,
            "Phone_Number": number, "doc_id":ref.id})
          .then((value) => {
            nameController.text = ""
            emailController.text = ""
            numberController.text = ""

          })
          .catchError((onError) => log(onError));
    }

    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            title: const Text(
              'Contact Form',
              style: TextStyle(color: Colors.green),
            )),
        body: SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                const SizedBox(
                  height: 40,
                ),
                TextFormField(
                    controller: nameController,
                    decoration: const InputDecoration(
                        hintText: 'Full names',
                        hintStyle: TextStyle(color: Colors.blueGrey),
                        labelText: 'Name',
                        labelStyle: TextStyle(color: Colors.green),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          borderSide: BorderSide(color: Colors.green),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            borderSide: BorderSide(color: Colors.green)),
                        icon: Icon(
                          Icons.person_add,
                          color: Colors.green,
                          size: 20,
                        ))),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                    controller: emailController,
                    decoration: const InputDecoration(
                      icon: Icon(
                        Icons.alternate_email,
                        color: Colors.green,
                        size: 20,
                      ),
                      hintText: 'Email address',
                      hintStyle: TextStyle(color: Colors.blueGrey),
                      labelText: 'Email',
                      labelStyle: TextStyle(color: Colors.green),
                  
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        borderSide: BorderSide(color: Colors.green),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          borderSide: BorderSide(color: Colors.green)),
                    )),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                    controller: numberController,
                    maxLength: 10,
                    decoration: const InputDecoration(
                      icon: Icon(
                        Icons.phone,
                        color: Colors.green,
                        size: 20,
                      ),
                      hintText: 'Phone Number',
                      hintStyle: TextStyle(color: Colors.blueGrey),
                      labelText: 'Number',
                      labelStyle: TextStyle(color: Colors.green),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        borderSide: BorderSide(color: Colors.green),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          borderSide: BorderSide(color: Colors.green)),
                    )),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.green,
                    onPrimary: Colors.black,
                  ),
                  onPressed: () {
                    _addSubmissions();
                  },
                  child: const Text('Add Submission'),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.only(left: 50.0, top: 20.0),
                ),
                 SubmissionList(); 
              ]            
            ),
        ));
  }
}
