import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

class SubmissionList extends StatefulWidget {
  const SubmissionList({Key? key}) : super(key: key);
  @override
  SubmissionListState createState() => SubmissionListState();
}

class SubmissionListState extends State<SubmissionList> {

  final Stream<QuerySnapshot> _mySubmissions = FirebaseFirestore.instance.collection("submissions").snapshots();  



  @override
  Widget build(BuildContext context) {
    TextEditingController _nameFieldCtrlr = TextEditingController();
    TextEditingController _emailFieldCtrlr = TextEditingController();
    TextEditingController _numberFieldCtrlr = TextEditingController();


    void _delete(docId) {
      FirebaseFirestore.instance
      .collection("submissions")
      .doc(docId)
      .delete()
      .then((value) ==> print('deleted'));

    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection('submissions');
      _nameFieldCtrlr.text = data['Full_Name'];
      _emailFieldCtrlr.text = data['Email_Address'];
      _numberFieldCtrlr.text = data['Phone_Number'];


      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text("Update"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextFiled(
                controller: _nameFieldCtrlr,
              ),
              TextFiled(
                controller: _emailFieldCtrlr,
              ),
              TextFiled(
                controller: _numberFieldCtrlr,
              ),
              TextButton(
                onPressed (){
                  collection.doc(data['doc_id'])
                  .update({'Full_Name': _nameFieldCtrlr.text,
                  .update({'Email_Address': _emailFieldCtrlr.text,
                  .update({'Phone_Number': _numberFieldCtrlr.text,


                  });
                  Navigator.pop(context);

                }, 
              
                child: Text('Update'))
            }
          )
        }
              )
      ]
      )

    }
      
    return StreamBuilder(
      stream: _mySubmissions, 
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Object?>> snapshot) { 
        if(snapshot.hasError){
          return Text("Something went wrong");
        }
        if(snapshot.connectionState == connectionState.waiting){
          return CircularProgressIndicator();
        }
        if(snapshot.hasData){
          return Row(
            children: [
              Expanded(child: SizedBox(
                height:(MediaQuery.of(context).size.height),
                width: MediaQuery.of(context).size.width,

                child: ListView(
                  children: 
                    snapshot.data!.docs.map((DocumentSnapshot documentSnapshot) async {
                      Map<String, dynamic> data = documentSnapshot.data() as Map<String, dynamic>;
                      return ListTile(
                        title: Text(data['Full_name']),
                        subtitle: Text(data['Email_Address']),
                        subtitle: Text(data['Phone_Number']),
                      ),
                      ButtonTheme(
                        child: ButtonBar(
                          children: [
                            OutlineButton:icon(
                              onPressed: (){
                                _update(data);
                              },
                              icon: Icon(Icons.edit),
                              label:Text ("Edit"),
                            ),
                            OutlineButton:icon(
                              onPressed: (){
                                _delete(data["doc_id"]);
                              },
                              icon: Icon(Icons.remove),
                              label:Text ("Delete"),
                              )
                          ],
                        )
                      )

                    })
                  )))
            ],
          );
        }else{
          return(Text('No data'));
        }
      }
    )
},
  }
