import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  ProfileState createState() => ProfileState();
}

class ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
                icon: const Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.pushNamed(context, '/four');
                })),

        body: SafeArea(
            child: Column(
          children: <Widget>[
            const CircleAvatar(
              radius: 60.0,
              backgroundImage: AssetImage('assets/profilepic.jpg'),
            ),
            const SizedBox(
              height: 40.0,
            ),
            Text(
              'Iman Maryam Morongwa Ngwepe-Ntshibida',
              style: Theme.of(context).textTheme.headline2,
            ),
            const SizedBox(
              height: 40.0,
            ),
            Text(
              'STUDENT',
              style: Theme.of(context).textTheme.bodyText1,
            ),
            const SizedBox(
              height: 40.0,
            ),
            Card(
              color: Colors.white,
              margin:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              child: ListTile(
                leading: const Icon(
                  Icons.phone,
                  color: Colors.teal,
                ),
                title: Text(
                  '083 588 7198',
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ),
            ),
            const SizedBox(
              height: 30.0,
            ),
            Card(
              color: Colors.white,
              margin:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              child: ListTile(
                leading: const Icon(
                  Icons.email,
                  color: Colors.teal,
                ),
                title: Text(
                  'deborah.ngwepe@gmail.com',
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ),
            ),
          ],
        )),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.green,
          child: const Icon(Icons.arrow_forward),
          onPressed: () {
            Navigator.pushNamed(context, '/last');
          },
        ));
  }
}
