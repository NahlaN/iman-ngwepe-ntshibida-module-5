import 'package:flutter/material.dart';

class Feature1 extends StatefulWidget {
  const Feature1({Key? key}) : super(key: key);
  @override
  Feature1State createState() => Feature1State();
}

class Feature1State extends State<Feature1> {
  @override
  Widget build(BuildContext context) {
    final Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    "We'd Love To Get In Touch With You",
                    style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
              ],),
            
                ),
              ],
            ),
          );

    final Color color = Theme.of(context).primaryColor;

    final Widget buttonSection = Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _buildButtonColumn(color, Icons.call, 'Call'),
          _buildButtonColumn(color, Icons.whatsapp, 'Whatsapp'),
          _buildButtonColumn(color, Icons.facebook, 'Facebook'),
        ]);

    final Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: const Text(
        'To give or take, proceed to the next page',
        style: TextStyle(fontFamily: 'Roboto', fontSize: 18, color: Colors.green),
        //softWrap: true,
      ),
    );

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          title: const Text('Feature',
          style: TextStyle(color: Colors.green),),
           leading: IconButton(

                icon: const Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.pushNamed(context, '/five');
                }),
        ),
        body: ListView(
          children: <Widget>[
            Image.asset(
              'assets/give.jpeg',
              fit: BoxFit.cover,
            ),
            titleSection,
            buttonSection,
            textSection,
          ],
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.green,
          child: const Icon(Icons.arrow_forward),
          onPressed: () {
            Navigator.pushNamed(context, '/form');
          }),
      ),
     
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
       
      ],
    );
  }
}
